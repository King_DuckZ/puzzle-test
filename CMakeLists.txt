cmake_minimum_required(VERSION 2.6)
project(PuzzleTest CXX C)

find_package(SDL REQUIRED)
find_package(SDL_image REQUIRED)
find_package(X11 REQUIRED)

if (CMAKE_BUILD_TYPE)
	string(TOUPPER ${CMAKE_BUILD_TYPE} buildTypeUpper)
	set(buildSuffix "_${buildTypeUpper}")
else()
	set(buildSuffix "")
	set(buildTypeUpper "UNKNOWN")
endif()

set(CMAKE_CXX_FLAGS${buildSuffix} "${CMAKE_CXX_FLAGS${buildSuffix}} -DPUZZLETEST_${buildTypeUpper} -Wall -pedantic -std=c++11")
set(CMAKE_C_FLAGS${buildSuffix} "${CMAKE_C_FLAGS${buildSuffix}} -DPUZZLETEST_${buildTypeUpper} -Wall -pedantic")

configure_file(
	"${PROJECT_SOURCE_DIR}/code/${PROJECT_NAME}Config.h.in"
	"${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.h"
)

file(GLOB graphicFiles "${CMAKE_CURRENT_SOURCE_DIR}/graphics/*.png")
install(FILES ${graphicFiles} DESTINATION graphics)

include_directories(
	${SDL_INCLUDE_DIR}
	"${PROJECT_BINARY_DIR}"
	${SDL_IMAGE_INCLUDE_DIRS}
	"${X11_INCLUDE_DIR}"
)

add_executable(${PROJECT_NAME}
	code/main.cpp
	code/game.cpp
	code/sdlrenderer.cpp
	code/state.cpp
	code/input.cpp
	code/eventmanager.cpp
	code/DuckCore/Functor.cpp
	code/gridwindow.cpp
)

target_link_libraries(${PROJECT_NAME}
	${SDL_LIBRARY}
	${SDL_IMAGE_LIBRARIES}
	${X11_LIBRARIES}
)
