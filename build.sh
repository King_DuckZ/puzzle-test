#!/usr/bin/env bash
mkdir build
if [ -d build ]; then
	cd build
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=$PWD ../
	make -j2 all install
	echo "All done, your files are in the \"build\" directory"
else
	echo "Sorry but I couldn't create the \"build\" directory"
	exit 1
fi
