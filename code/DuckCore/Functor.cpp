#include "../main.hpp"
//--------------Warning!--------------------------------------------------------
//This file was generated using Functor.rb. Please do not edit it by hand.

#include "Functor.hpp"
#include <algorithm>

#define Assert(a) assert(a)
#define AssertRelease(a) assert(a)

// #define DEBUG_VERBOSE

#ifdef DEBUG_VERBOSE
#include <iostream>
#endif

namespace duckcore {
	namespace implem {
		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		FunctorBase::FunctorBase() {
			//If the following condition is not true, this class cannot be
			//used on this architecture without changing it.
			static_assert(sizeof(MethodPtrType) >= sizeof(const void*), "Method pointer smaller than function pointer");

			std::fill(m_MethodPtr, m_MethodPtr + sizeof(m_MethodPtr), 0);
			m_Object = nullptr;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		FunctorBase::FunctorBase (const void* parObject, const void* parCallback, size_t parCallbackSize) {
			if (parObject) {
				AssertRelease(parCallbackSize <= sizeof(m_MethodPtr));
				m_Object = parObject;
				const char* charLikeCallback = static_cast<const char*>(parCallback);
				std::copy(charLikeCallback, charLikeCallback + parCallbackSize, m_MethodPtr);
				Assert(m_Object == parObject);
			}
			else {
				m_Object = nullptr;
				m_FreeFuncPtr = parCallback;
				Assert(nullptr == m_Object);
			}
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		FunctorBase::operator bool() const {
			return static_cast<bool>(m_Object or m_FreeFuncPtr);
		}
	} //namespace implem

#ifdef WITH_DEBUG_TESTS
	namespace debug {
		namespace {
			int IntCallback() {
#ifdef DEBUG_VERBOSE
				std::cout << "DEBUG: int IntCallback()\n";
#endif
				return 0;
			}

			int IntCallbackOneArg (const std::string& parSay) {
#ifdef WITH_DEBUG_TESTS
				std::cout << "DEBUG: int IntCallbackOneArg (const std::string& parSay)\n" << parSay << "\n";
#endif
				//This is just to avoid warnings about unused variables
				if (parSay.empty())
					return 0;
				else
					return 1;
			}

			class CMemberCallback {
			public:
				void NonConstVoid ( void ) {
#ifdef DEBUG_VERBOSE
					std::cout << "DEBUG: CMemberCallback::NonConstVoid()\n";
#endif
				}
				void ConstVoid ( void ) const {
#ifdef DEBUG_VERBOSE
					std::cout << "DEBUG: CMemberCallback::ConstVoid()\n";
#endif
				}
// 				float NonConstFloat ( double, int, short ) { return 0.0f; }
// 				float ConstFloat ( double, int, short ) const { return 0.0f; }
			};
		} //unnamed namespace
	} //namespace debug

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool RunFunctorDebugTests() {
#ifdef DEBUG_VERBOSE
		std::cout << "Testing Functors.hpp\n";
#endif

		using namespace debug;
		{
			Functor0<int> func;
			MakeFunctor(func, &IntCallback);
			func();
		}
		{
			Functor1<int, const std::string&> func;
			MakeFunctor(func, &IntCallbackOneArg);
			std::string testString1("DEBUG: Test string passed to func");
			func(testString1);

			Functor1<int, const std::string&> funcWrapper;
			MakeFunctor(funcWrapper, &func);
			std::string testString2("DEBUG: Test string passed to funcWrapper");
			funcWrapper(testString2);
		}
		{
			CMemberCallback dummy;
			Functor0<void> funcNonConst;
			MakeFunctor(funcNonConst, &CMemberCallback::NonConstVoid, dummy);
			funcNonConst();

			Functor0<void> funcConst;
			MakeFunctor(funcConst, &CMemberCallback::ConstVoid, dummy);
			funcConst();
		}
#ifdef DEBUG_VERBOSE
		std::cout << std::endl;
#endif
		return true;
	}
#endif
} //namespace duckcore

#ifdef DEBUG_VERBOSE
#undef DEBUG_VERBOSE
#endif
#undef Assert
#undef AssertRelease
