#ifndef idFAA8EC9EFC2411E090C70023545F154C
#define idFAA8EC9EFC2411E090C70023545F154C

#include <functional>

//--------------Warning!--------------------------------------------------------
//This file was generated using Functor.rb. Please do not edit it by hand.

//#include <loki/TypeTraits.h>
#if defined(COMPLY_TO_LOKI_FUNCTOR)
#include <loki/Typelist.h>
#endif

namespace duckcore {
#if defined(COMPLY_TO_LOKI_FUNCTOR)
	template <typename R, typename TList> class Functor;
#endif

	///-------------------------------------------------------------------------
	///Factory function for wrapping free functions and callable objects.
	///-------------------------------------------------------------------------
	template <typename _FunctorType, typename _CallbackType>
	void MakeFunctor ( _FunctorType& parFunctor, _CallbackType parCallback );

	///-------------------------------------------------------------------------
	///Factory function for wrapping methods and callable properties.
	///-------------------------------------------------------------------------
	template <typename _FunctorType, typename _CallbackType, typename _ObjType>
	void MakeFunctor ( _FunctorType& parFunctor, const _CallbackType& parCallback, _ObjType& parObject );

#ifdef WITH_DEBUG_TESTS
	///-------------------------------------------------------------------------
	///Run debug tests.
	///-------------------------------------------------------------------------
	bool RunFunctorDebugTests ( void );
#endif

	namespace implem {
		///---------------------------------------------------------------------
		///Protected base class for all Functor classes.
		///---------------------------------------------------------------------
		class FunctorBase {
		protected:
			typedef void (FunctorBase::*MethodPtrType)();

			FunctorBase ( void );
			FunctorBase ( const void* parObject, const void* parCallback, size_t parCallbackSize );

			operator bool ( void ) const;

		public:
			union {
				const void* m_FreeFuncPtr;
				char m_MethodPtr[sizeof(MethodPtrType)];
			};
			const void* m_Object;
		};

		///---------------------------------------------------------------------
		///Convenience template to select the translator corresponding to a
		///given Functor class.
		///---------------------------------------------------------------------
		template <typename _FunctorType, typename _CallbType, typename _ObjType> struct HelperFunctorTranslator;
	} //namespace implem
	///-------------------------------------------------------------------------
	///Functor taking no parameters.
	///-------------------------------------------------------------------------
	template <typename TRet>
	class Functor0 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 0
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef Loki::NullType ParmList;
#endif
		Functor0 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor0 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor0 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( void ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&);
		Functor0 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking one parameter.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1>
	class Functor1 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 1
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_1(TP1) ParmList;
#endif
		Functor1 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor1 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor1 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1);
		Functor1 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking two parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2>
	class Functor2 : protected implem::FunctorBase, public std::binary_function<TP1, TP2, TRet> {
	public:
		enum {
			ParametersCount = 2
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_2(TP1, TP2) ParmList;
#endif
		Functor2 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor2 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor2 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2);
		Functor2 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking three parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3>
	class Functor3 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 3
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_3(TP1, TP2, TP3) ParmList;
#endif
		Functor3 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor3 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor3 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3);
		Functor3 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking four parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4>
	class Functor4 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 4
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_4(TP1, TP2, TP3, TP4) ParmList;
#endif
		Functor4 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor4 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor4 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4);
		Functor4 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking five parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5>
	class Functor5 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 5
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_5(TP1, TP2, TP3, TP4, TP5) ParmList;
#endif
		Functor5 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor5 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor5 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5);
		Functor5 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking six parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6>
	class Functor6 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 6
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_6(TP1, TP2, TP3, TP4, TP5, TP6) ParmList;
#endif
		Functor6 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor6 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor6 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6);
		Functor6 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking seven parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7>
	class Functor7 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 7
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_7(TP1, TP2, TP3, TP4, TP5, TP6, TP7) ParmList;
#endif
		Functor7 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor7 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor7 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7);
		Functor7 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking eight parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8>
	class Functor8 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 8
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_8(TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8) ParmList;
#endif
		Functor8 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor8 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor8 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8);
		Functor8 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking nine parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9>
	class Functor9 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 9
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_9(TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9) ParmList;
#endif
		Functor9 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor9 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor9 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9);
		Functor9 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking ten parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10>
	class Functor10 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 10
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_10(TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10) ParmList;
#endif
		Functor10 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor10 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor10 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10);
		Functor10 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking eleven parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11>
	class Functor11 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 11
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_11(TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11) ParmList;
#endif
		Functor11 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor11 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor11 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11);
		Functor11 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking twelve parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12>
	class Functor12 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 12
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_12(TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12) ParmList;
#endif
		Functor12 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor12 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor12 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12);
		Functor12 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking thirteen parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13>
	class Functor13 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 13
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_13(TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13) ParmList;
#endif
		Functor13 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor13 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor13 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13);
		Functor13 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking fourteen parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14>
	class Functor14 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 14
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_14(TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14) ParmList;
#endif
		Functor14 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor14 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor14 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14);
		Functor14 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking fifteen parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15>
	class Functor15 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 15
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_15(TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15) ParmList;
#endif
		Functor15 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor15 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor15 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15);
		Functor15 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	///-------------------------------------------------------------------------
	///Functor taking sixteen parameters.
	///-------------------------------------------------------------------------
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename TP16>
	class Functor16 : protected implem::FunctorBase {
	public:
		enum {
			ParametersCount = 16
		};
		typedef TRet ResultType; //This name is compliant with Loki's Functor
#if defined(COMPLY_TO_LOKI_FUNCTOR)
		typedef LOKI_TYPELIST_16(TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16) ParmList;
#endif
		Functor16 ( void ) : m_Thunk(nullptr) { return; }
		template <typename _CallbType> explicit Functor16 ( _CallbType parCallback ) { MakeFunctor(*this, parCallback); }
		template <typename _CallbType, typename _ObjType> Functor16 ( _CallbType parCallback, _ObjType& parObject ) { MakeFunctor(*this, parCallback, parObject); }
		using implem::FunctorBase::operator bool;
		TRet operator() ( TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15, TP16 parArg16 ) const;

	protected:
		typedef TRet (*ThunkType)(const implem::FunctorBase&, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16);
		Functor16 ( ThunkType t, const void* obj, const void* callback, size_t sz ) : implem::FunctorBase(obj, callback, sz), m_Thunk(t) { return; }

	private:
		ThunkType m_Thunk;
	};

	namespace implem {
		///---------------------------------------------------------------------
		///Translators for functors with no parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename _CallbType>
		class FunctionTranslator0 : public Functor0<TRet> {
		public:
			explicit FunctionTranslator0 ( _CallbType parCallback ) : Functor0<TRet>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(); }
		};
		template <typename TRet, typename _CallbType, typename _ObjType>
		class MethodTranslator0 : public Functor0<TRet> {
		public:
			MethodTranslator0 ( _CallbType f, _ObjType& obj ) : Functor0<TRet>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor );
		};

		///---------------------------------------------------------------------
		///Translators for functors with one parameter.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename _CallbType>
		class FunctionTranslator1 : public Functor1<TRet, TP1> {
		public:
			explicit FunctionTranslator1 ( _CallbType parCallback ) : Functor1<TRet, TP1>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1); }
		};
		template <typename TRet, typename TP1, typename _CallbType, typename _ObjType>
		class MethodTranslator1 : public Functor1<TRet, TP1> {
		public:
			MethodTranslator1 ( _CallbType f, _ObjType& obj ) : Functor1<TRet, TP1>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with two parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename _CallbType>
		class FunctionTranslator2 : public Functor2<TRet, TP1, TP2> {
		public:
			explicit FunctionTranslator2 ( _CallbType parCallback ) : Functor2<TRet, TP1, TP2>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2); }
		};
		template <typename TRet, typename TP1, typename TP2, typename _CallbType, typename _ObjType>
		class MethodTranslator2 : public Functor2<TRet, TP1, TP2> {
		public:
			MethodTranslator2 ( _CallbType f, _ObjType& obj ) : Functor2<TRet, TP1, TP2>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with three parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename _CallbType>
		class FunctionTranslator3 : public Functor3<TRet, TP1, TP2, TP3> {
		public:
			explicit FunctionTranslator3 ( _CallbType parCallback ) : Functor3<TRet, TP1, TP2, TP3>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename _CallbType, typename _ObjType>
		class MethodTranslator3 : public Functor3<TRet, TP1, TP2, TP3> {
		public:
			MethodTranslator3 ( _CallbType f, _ObjType& obj ) : Functor3<TRet, TP1, TP2, TP3>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with four parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename _CallbType>
		class FunctionTranslator4 : public Functor4<TRet, TP1, TP2, TP3, TP4> {
		public:
			explicit FunctionTranslator4 ( _CallbType parCallback ) : Functor4<TRet, TP1, TP2, TP3, TP4>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename _CallbType, typename _ObjType>
		class MethodTranslator4 : public Functor4<TRet, TP1, TP2, TP3, TP4> {
		public:
			MethodTranslator4 ( _CallbType f, _ObjType& obj ) : Functor4<TRet, TP1, TP2, TP3, TP4>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with five parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename _CallbType>
		class FunctionTranslator5 : public Functor5<TRet, TP1, TP2, TP3, TP4, TP5> {
		public:
			explicit FunctionTranslator5 ( _CallbType parCallback ) : Functor5<TRet, TP1, TP2, TP3, TP4, TP5>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename _CallbType, typename _ObjType>
		class MethodTranslator5 : public Functor5<TRet, TP1, TP2, TP3, TP4, TP5> {
		public:
			MethodTranslator5 ( _CallbType f, _ObjType& obj ) : Functor5<TRet, TP1, TP2, TP3, TP4, TP5>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with six parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename _CallbType>
		class FunctionTranslator6 : public Functor6<TRet, TP1, TP2, TP3, TP4, TP5, TP6> {
		public:
			explicit FunctionTranslator6 ( _CallbType parCallback ) : Functor6<TRet, TP1, TP2, TP3, TP4, TP5, TP6>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename _CallbType, typename _ObjType>
		class MethodTranslator6 : public Functor6<TRet, TP1, TP2, TP3, TP4, TP5, TP6> {
		public:
			MethodTranslator6 ( _CallbType f, _ObjType& obj ) : Functor6<TRet, TP1, TP2, TP3, TP4, TP5, TP6>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with seven parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename _CallbType>
		class FunctionTranslator7 : public Functor7<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7> {
		public:
			explicit FunctionTranslator7 ( _CallbType parCallback ) : Functor7<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename _CallbType, typename _ObjType>
		class MethodTranslator7 : public Functor7<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7> {
		public:
			MethodTranslator7 ( _CallbType f, _ObjType& obj ) : Functor7<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with eight parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename _CallbType>
		class FunctionTranslator8 : public Functor8<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8> {
		public:
			explicit FunctionTranslator8 ( _CallbType parCallback ) : Functor8<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename _CallbType, typename _ObjType>
		class MethodTranslator8 : public Functor8<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8> {
		public:
			MethodTranslator8 ( _CallbType f, _ObjType& obj ) : Functor8<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with nine parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename _CallbType>
		class FunctionTranslator9 : public Functor9<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9> {
		public:
			explicit FunctionTranslator9 ( _CallbType parCallback ) : Functor9<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename _CallbType, typename _ObjType>
		class MethodTranslator9 : public Functor9<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9> {
		public:
			MethodTranslator9 ( _CallbType f, _ObjType& obj ) : Functor9<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with ten parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename _CallbType>
		class FunctionTranslator10 : public Functor10<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10> {
		public:
			explicit FunctionTranslator10 ( _CallbType parCallback ) : Functor10<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename _CallbType, typename _ObjType>
		class MethodTranslator10 : public Functor10<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10> {
		public:
			MethodTranslator10 ( _CallbType f, _ObjType& obj ) : Functor10<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with eleven parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename _CallbType>
		class FunctionTranslator11 : public Functor11<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11> {
		public:
			explicit FunctionTranslator11 ( _CallbType parCallback ) : Functor11<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename _CallbType, typename _ObjType>
		class MethodTranslator11 : public Functor11<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11> {
		public:
			MethodTranslator11 ( _CallbType f, _ObjType& obj ) : Functor11<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with twelve parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename _CallbType>
		class FunctionTranslator12 : public Functor12<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12> {
		public:
			explicit FunctionTranslator12 ( _CallbType parCallback ) : Functor12<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename _CallbType, typename _ObjType>
		class MethodTranslator12 : public Functor12<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12> {
		public:
			MethodTranslator12 ( _CallbType f, _ObjType& obj ) : Functor12<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with thirteen parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename _CallbType>
		class FunctionTranslator13 : public Functor13<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13> {
		public:
			explicit FunctionTranslator13 ( _CallbType parCallback ) : Functor13<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename _CallbType, typename _ObjType>
		class MethodTranslator13 : public Functor13<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13> {
		public:
			MethodTranslator13 ( _CallbType f, _ObjType& obj ) : Functor13<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with fourteen parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename _CallbType>
		class FunctionTranslator14 : public Functor14<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14> {
		public:
			explicit FunctionTranslator14 ( _CallbType parCallback ) : Functor14<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13, parArg14); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename _CallbType, typename _ObjType>
		class MethodTranslator14 : public Functor14<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14> {
		public:
			MethodTranslator14 ( _CallbType f, _ObjType& obj ) : Functor14<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with fifteen parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename _CallbType>
		class FunctionTranslator15 : public Functor15<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15> {
		public:
			explicit FunctionTranslator15 ( _CallbType parCallback ) : Functor15<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13, parArg14, parArg15); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename _CallbType, typename _ObjType>
		class MethodTranslator15 : public Functor15<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15> {
		public:
			MethodTranslator15 ( _CallbType f, _ObjType& obj ) : Functor15<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15 );
		};

		///---------------------------------------------------------------------
		///Translators for functors with sixteen parameters.
		///---------------------------------------------------------------------
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename TP16, typename _CallbType>
		class FunctionTranslator16 : public Functor16<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16> {
		public:
			explicit FunctionTranslator16 ( _CallbType parCallback ) : Functor16<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16>(&Thunk, nullptr, reinterpret_cast<const void*>(parCallback), sizeof(_CallbType)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15, TP16 parArg16 ) { return (*reinterpret_cast<_CallbType>(const_cast<void*>(functor.m_FreeFuncPtr)))(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13, parArg14, parArg15, parArg16); }
		};
		template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename TP16, typename _CallbType, typename _ObjType>
		class MethodTranslator16 : public Functor16<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16> {
		public:
			MethodTranslator16 ( _CallbType f, _ObjType& obj ) : Functor16<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16>(&Thunk, &obj, &f, sizeof(f)) { return; }
			static TRet Thunk ( const FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15, TP16 parArg16 );
		};

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <template <typename> class _Functor, typename TRet, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet>, _CallbType, _ObjType> {
			typedef FunctionTranslator0<TRet, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator0<TRet, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename> class _Functor, typename TRet, typename TP1, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1>, _CallbType, _ObjType> {
			typedef FunctionTranslator1<TRet, TP1, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator1<TRet, TP1, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2>, _CallbType, _ObjType> {
			typedef FunctionTranslator2<TRet, TP1, TP2, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator2<TRet, TP1, TP2, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3>, _CallbType, _ObjType> {
			typedef FunctionTranslator3<TRet, TP1, TP2, TP3, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator3<TRet, TP1, TP2, TP3, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4>, _CallbType, _ObjType> {
			typedef FunctionTranslator4<TRet, TP1, TP2, TP3, TP4, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator4<TRet, TP1, TP2, TP3, TP4, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5>, _CallbType, _ObjType> {
			typedef FunctionTranslator5<TRet, TP1, TP2, TP3, TP4, TP5, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator5<TRet, TP1, TP2, TP3, TP4, TP5, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6>, _CallbType, _ObjType> {
			typedef FunctionTranslator6<TRet, TP1, TP2, TP3, TP4, TP5, TP6, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator6<TRet, TP1, TP2, TP3, TP4, TP5, TP6, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7>, _CallbType, _ObjType> {
			typedef FunctionTranslator7<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator7<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8>, _CallbType, _ObjType> {
			typedef FunctionTranslator8<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator8<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9>, _CallbType, _ObjType> {
			typedef FunctionTranslator9<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator9<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10>, _CallbType, _ObjType> {
			typedef FunctionTranslator10<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator10<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11>, _CallbType, _ObjType> {
			typedef FunctionTranslator11<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator11<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12>, _CallbType, _ObjType> {
			typedef FunctionTranslator12<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator12<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13>, _CallbType, _ObjType> {
			typedef FunctionTranslator13<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator13<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14>, _CallbType, _ObjType> {
			typedef FunctionTranslator14<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator14<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15>, _CallbType, _ObjType> {
			typedef FunctionTranslator15<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator15<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, _CallbType, _ObjType> MethodTranslatorType;
		};
		template <template <typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename, typename> class _Functor, typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename TP16, typename _ObjType, typename _CallbType>
		struct HelperFunctorTranslator<_Functor<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16>, _CallbType, _ObjType> {
			typedef FunctionTranslator16<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16, _CallbType> FunctionTranslatorType;
			typedef MethodTranslator16<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16, _CallbType, _ObjType> MethodTranslatorType;
		};
	} //namespace implem
#if defined(COMPLY_TO_LOKI_FUNCTOR)
	namespace Implem {
		template <typename R, typename TList, int Count=Loki::TL::Length<TList>::value>
		class FunctorTypeFromTList;
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 0> {
		public:
			typedef Functor0<R> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 1> {
		public:
			typedef Functor1<R, typename Loki::TL::TypeAt<TList, 0>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 2> {
		public:
			typedef Functor2<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 3> {
		public:
			typedef Functor3<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 4> {
		public:
			typedef Functor4<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 5> {
		public:
			typedef Functor5<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 6> {
		public:
			typedef Functor6<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 7> {
		public:
			typedef Functor7<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 8> {
		public:
			typedef Functor8<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result, typename Loki::TL::TypeAt<TList, 7>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 9> {
		public:
			typedef Functor9<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result, typename Loki::TL::TypeAt<TList, 7>::Result, typename Loki::TL::TypeAt<TList, 8>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 10> {
		public:
			typedef Functor10<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result, typename Loki::TL::TypeAt<TList, 7>::Result, typename Loki::TL::TypeAt<TList, 8>::Result, typename Loki::TL::TypeAt<TList, 9>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 11> {
		public:
			typedef Functor11<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result, typename Loki::TL::TypeAt<TList, 7>::Result, typename Loki::TL::TypeAt<TList, 8>::Result, typename Loki::TL::TypeAt<TList, 9>::Result, typename Loki::TL::TypeAt<TList, 10>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 12> {
		public:
			typedef Functor12<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result, typename Loki::TL::TypeAt<TList, 7>::Result, typename Loki::TL::TypeAt<TList, 8>::Result, typename Loki::TL::TypeAt<TList, 9>::Result, typename Loki::TL::TypeAt<TList, 10>::Result, typename Loki::TL::TypeAt<TList, 11>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 13> {
		public:
			typedef Functor13<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result, typename Loki::TL::TypeAt<TList, 7>::Result, typename Loki::TL::TypeAt<TList, 8>::Result, typename Loki::TL::TypeAt<TList, 9>::Result, typename Loki::TL::TypeAt<TList, 10>::Result, typename Loki::TL::TypeAt<TList, 11>::Result, typename Loki::TL::TypeAt<TList, 12>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 14> {
		public:
			typedef Functor14<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result, typename Loki::TL::TypeAt<TList, 7>::Result, typename Loki::TL::TypeAt<TList, 8>::Result, typename Loki::TL::TypeAt<TList, 9>::Result, typename Loki::TL::TypeAt<TList, 10>::Result, typename Loki::TL::TypeAt<TList, 11>::Result, typename Loki::TL::TypeAt<TList, 12>::Result, typename Loki::TL::TypeAt<TList, 13>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 15> {
		public:
			typedef Functor15<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result, typename Loki::TL::TypeAt<TList, 7>::Result, typename Loki::TL::TypeAt<TList, 8>::Result, typename Loki::TL::TypeAt<TList, 9>::Result, typename Loki::TL::TypeAt<TList, 10>::Result, typename Loki::TL::TypeAt<TList, 11>::Result, typename Loki::TL::TypeAt<TList, 12>::Result, typename Loki::TL::TypeAt<TList, 13>::Result, typename Loki::TL::TypeAt<TList, 14>::Result> Result;
		};
		template <typename R, typename TList>
		class FunctorTypeFromTList<R, TList, 16> {
		public:
			typedef Functor16<R, typename Loki::TL::TypeAt<TList, 0>::Result, typename Loki::TL::TypeAt<TList, 1>::Result, typename Loki::TL::TypeAt<TList, 2>::Result, typename Loki::TL::TypeAt<TList, 3>::Result, typename Loki::TL::TypeAt<TList, 4>::Result, typename Loki::TL::TypeAt<TList, 5>::Result, typename Loki::TL::TypeAt<TList, 6>::Result, typename Loki::TL::TypeAt<TList, 7>::Result, typename Loki::TL::TypeAt<TList, 8>::Result, typename Loki::TL::TypeAt<TList, 9>::Result, typename Loki::TL::TypeAt<TList, 10>::Result, typename Loki::TL::TypeAt<TList, 11>::Result, typename Loki::TL::TypeAt<TList, 12>::Result, typename Loki::TL::TypeAt<TList, 13>::Result, typename Loki::TL::TypeAt<TList, 14>::Result, typename Loki::TL::TypeAt<TList, 15>::Result> Result;
		};
	} //namespace Implem
#endif
#if defined(COMPLY_TO_LOKI_FUNCTOR)
	template <typename R, typename TList>
	class Functor {
	public:
		typedef typename Implem::FunctorTypeFromTList<R, TList>::Result FunctorType;
		typedef typename FunctorType::ResultType ResultType;
		enum {
			ParametersCount = Loki::TL::Length<TList>::value
		};

		Functor ( void ) { }
		Functor ( const Functor& parOther ) : m_functor(parOther.m_functor) { }
		explicit Functor ( const FunctorType& parFunctor ) : m_functor(parFunctor) { }
		~Functor ( void ) { }

		const Functor& operator= ( const Functor& parOther ) { m_functor = parOther.m_functor; return *this; }
		const Functor& operator= ( const FunctorType& parOther ) { m_functor = parOther; return *this; }

		operator FunctorType& ( void ) { return m_functor; }

		template
		ResultType operator() ( void ) { return m_functor(); }
		template <typename TP1>
		ResultType operator() ( TP1 parA1 ) { return m_functor(parA1); }
		template <typename TP1, typename TP2>
		ResultType operator() ( TP1 parA1, TP2 parA2 ) { return m_functor(parA1, parA2); }
		template <typename TP1, typename TP2, typename TP3>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3 ) { return m_functor(parA1, parA2, parA3); }
		template <typename TP1, typename TP2, typename TP3, typename TP4>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4 ) { return m_functor(parA1, parA2, parA3, parA4); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5 ) { return m_functor(parA1, parA2, parA3, parA4, parA5); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7, TP8 parA8 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7, parA8); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7, TP8 parA8, TP9 parA9 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7, parA8, parA9); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7, TP8 parA8, TP9 parA9, TP10 parA10 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7, parA8, parA9, parA10); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7, TP8 parA8, TP9 parA9, TP10 parA10, TP11 parA11 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7, parA8, parA9, parA10, parA11); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7, TP8 parA8, TP9 parA9, TP10 parA10, TP11 parA11, TP12 parA12 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7, parA8, parA9, parA10, parA11, parA12); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7, TP8 parA8, TP9 parA9, TP10 parA10, TP11 parA11, TP12 parA12, TP13 parA13 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7, parA8, parA9, parA10, parA11, parA12, parA13); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7, TP8 parA8, TP9 parA9, TP10 parA10, TP11 parA11, TP12 parA12, TP13 parA13, TP14 parA14 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7, parA8, parA9, parA10, parA11, parA12, parA13, parA14); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7, TP8 parA8, TP9 parA9, TP10 parA10, TP11 parA11, TP12 parA12, TP13 parA13, TP14 parA14, TP15 parA15 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7, parA8, parA9, parA10, parA11, parA12, parA13, parA14, parA15); }
		template <typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename TP16>
		ResultType operator() ( TP1 parA1, TP2 parA2, TP3 parA3, TP4 parA4, TP5 parA5, TP6 parA6, TP7 parA7, TP8 parA8, TP9 parA9, TP10 parA10, TP11 parA11, TP12 parA12, TP13 parA13, TP14 parA14, TP15 parA15, TP16 parA16 ) { return m_functor(parA1, parA2, parA3, parA4, parA5, parA6, parA7, parA8, parA9, parA10, parA11, parA12, parA13, parA14, parA15, parA16); }
	private:
		FunctorType m_functor;
	};
#endif
} //namespace duckcore
#include "Functor.inl"
#endif
