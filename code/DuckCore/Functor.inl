//--------------Warning!--------------------------------------------------------
//This file was generated using Functor.rb. Please do not edit it by hand.

namespace duckcore {
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	template <typename _FunctorType, typename _CallbackType>
	inline
	void MakeFunctor (_FunctorType& parFunctor, _CallbackType parCallback) {
		typedef typename implem::HelperFunctorTranslator<_FunctorType, _CallbackType, void>::FunctionTranslatorType TranslatorType;
		TranslatorType tmpRet(parCallback);
		parFunctor = tmpRet;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	template <typename _FunctorType, typename _CallbackType, typename _ObjType>
	inline
	void MakeFunctor (_FunctorType& parFunctor, const _CallbackType& parCallback, _ObjType& parObject) {
		typedef typename implem::HelperFunctorTranslator<_FunctorType, _CallbackType, _ObjType>::MethodTranslatorType TranslatorType;
		TranslatorType tmpRet(parCallback, parObject);
		parFunctor = tmpRet;
	}
	template <typename TRet, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator0<TRet, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)();
	}
	template <typename TRet>
	TRet Functor0<TRet>::operator()() const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this);
	}
	template <typename TRet, typename TP1, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator1<TRet, TP1, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1);
	}
	template <typename TRet, typename TP1>
	TRet Functor1<TRet, TP1>::operator() (TP1 parArg1) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1);
	}
	template <typename TRet, typename TP1, typename TP2, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator2<TRet, TP1, TP2, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2);
	}
	template <typename TRet, typename TP1, typename TP2>
	TRet Functor2<TRet, TP1, TP2>::operator() (TP1 parArg1, TP2 parArg2) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator3<TRet, TP1, TP2, TP3, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3>
	TRet Functor3<TRet, TP1, TP2, TP3>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator4<TRet, TP1, TP2, TP3, TP4, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4>
	TRet Functor4<TRet, TP1, TP2, TP3, TP4>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator5<TRet, TP1, TP2, TP3, TP4, TP5, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5>
	TRet Functor5<TRet, TP1, TP2, TP3, TP4, TP5>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator6<TRet, TP1, TP2, TP3, TP4, TP5, TP6, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6>
	TRet Functor6<TRet, TP1, TP2, TP3, TP4, TP5, TP6>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator7<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7>
	TRet Functor7<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator8<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8>
	TRet Functor8<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator9<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9>
	TRet Functor9<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator10<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10>
	TRet Functor10<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator11<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11>
	TRet Functor11<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator12<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12>
	TRet Functor12<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator13<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13>
	TRet Functor13<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator14<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13, parArg14);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14>
	TRet Functor14<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13, parArg14);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator15<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13, parArg14, parArg15);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15>
	TRet Functor15<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13, parArg14, parArg15);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename TP16, typename _CallbType, typename _ObjType>
	TRet implem::MethodTranslator16<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16, _CallbType, _ObjType>::Thunk (const implem::FunctorBase& functor, TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15, TP16 parArg16) {
		_ObjType* callee = reinterpret_cast<_ObjType*>(const_cast<void*>(functor.m_Object));
		_CallbType& memFunc = *reinterpret_cast<_CallbType*>(const_cast<char*>(functor.m_MethodPtr));
		return (callee->*memFunc)(parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13, parArg14, parArg15, parArg16);
	}
	template <typename TRet, typename TP1, typename TP2, typename TP3, typename TP4, typename TP5, typename TP6, typename TP7, typename TP8, typename TP9, typename TP10, typename TP11, typename TP12, typename TP13, typename TP14, typename TP15, typename TP16>
	TRet Functor16<TRet, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, TP10, TP11, TP12, TP13, TP14, TP15, TP16>::operator() (TP1 parArg1, TP2 parArg2, TP3 parArg3, TP4 parArg4, TP5 parArg5, TP6 parArg6, TP7 parArg7, TP8 parArg8, TP9 parArg9, TP10 parArg10, TP11 parArg11, TP12 parArg12, TP13 parArg13, TP14 parArg14, TP15 parArg15, TP16 parArg16) const {
		Assert(NULL != m_Thunk);
		return m_Thunk(*this, parArg1, parArg2, parArg3, parArg4, parArg5, parArg6, parArg7, parArg8, parArg9, parArg10, parArg11, parArg12, parArg13, parArg14, parArg15, parArg16);
	}
} //namespace duckcore
