#include "main.hpp"
#include "eventmanager.hpp"
#include <SDL.h>
#include "eventwrapper.hpp"

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
EventManager::EventManager() {
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
EventManager::~EventManager() {
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
bool EventManager::DoEvents() {
	SDL_Event event;
	if (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			return true;
		}
	}

	const EventWrapper eventWrapper(event);
	for (auto itCallb = m_callbacks.begin(), itCallbEnd = m_callbacks.end(); itCallb != itCallbEnd; ++itCallb) {
		if ((*itCallb)(eventWrapper))
			return true;
	}
	return false;
}
