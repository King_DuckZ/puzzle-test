#ifndef id1CA0FF52100545698C867DEF9BD8F9B7
#define id1CA0FF52100545698C867DEF9BD8F9B7

#if !defined(Assert)
#define Assert(a) assert(a)
#define UNDEF_ASSERT
#endif
#include "DuckCore/Functor.hpp"
#if defined(UNDEF_ASSERT)
#undef UNDEF_ASSERT
#undef Assert
#endif

struct EventWrapper;

class EventManager {
public:
	typedef duckcore::Functor1<bool, const EventWrapper&> DoEventsCallbackType;

	EventManager ( void );
	~EventManager ( void );

	bool DoEvents ( void );
	void RegisterCallback ( const DoEventsCallbackType& parCallback ) { m_callbacks.push_back(parCallback); }

private:
	std::vector<DoEventsCallbackType> m_callbacks;
};

#endif
