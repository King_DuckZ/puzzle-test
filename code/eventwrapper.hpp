#ifndef idFA8733AA6BAB4FC2BFFCE6A33E6B1519
#define idFA8733AA6BAB4FC2BFFCE6A33E6B1519

#include <SDL.h>

struct EventWrapper {
	EventWrapper ( SDL_Event& parEvent ) : event(parEvent) {}
	const EventWrapper& operator= ( const EventWrapper& ) = delete;
	SDL_Event& event;
};

#endif
