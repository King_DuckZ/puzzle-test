#include "main.hpp"
#include "game.hpp"
#include "state.hpp"
#include "input.hpp"
#include "gridpixelinfo.hpp"
#include <iostream>
#include <cmath>

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
Game::Game (const GridPixelInfo* parGridInfo, uint8_t parGemCount) :
	m_state(new GameState(parGridInfo, parGemCount)),
	m_swappingFrom(0),
	m_isSwapping(false)
{
	m_state->FillRandom();
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
Game::~Game() {
	delete m_state;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
bool Game::UpdateFrame (float parDelta, const MouseInput& parMouse) {
	const GridPixelInfo& gridInfo = m_state->GetGridInfo();

	if (parMouse.GetLeftButtonPressed()) {
		const uint2& mousePos = parMouse.GetPosition();
		if (PointIsInGrid(gridInfo, mousePos)) {
			const ushort2 cell(GetCellFromCoordinates(gridInfo, mousePos));
			std::cout << "You clicked cell " << cell << std::endl;
			if (m_isSwapping)
				SwapCells(m_swappingFrom, cell);
			else
				m_swappingFrom = cell;
			m_isSwapping = not m_isSwapping;
		}
	}
	return true;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
bool Game::SwapCells (const ushort2& parFrom, const ushort2& parTo) {
	std::cout << "Asked to swap " << parFrom << " and " << parTo << ": ";
	bool allowSwap;
	if (parFrom.x() == parTo.x() and std::abs(static_cast<int32_t>(parFrom.y()) - static_cast<int32_t>(parTo.y())) == 1 or
		parFrom.y() == parTo.y() and std::abs(static_cast<int32_t>(parFrom.x()) - static_cast<int32_t>(parTo.x())) == 1)
	{
		std::cout << "Ok!";
		allowSwap = true;
		m_state->SwapCells(parFrom, parTo);
	}
	else {
		std::cout << "Invalid!";
		allowSwap = false;
	}
	std::cout << std::endl;
	return allowSwap;
}
