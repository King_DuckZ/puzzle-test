#ifndef id8222525F68BE44A0BA976075737A910D
#define id8222525F68BE44A0BA976075737A910D

class GameState;
class MouseInput;
struct GridPixelInfo;

class Game {
public:
	Game ( const GridPixelInfo* parGridInfo, uint8_t parGemCount );
	~Game ( void );

	bool UpdateFrame ( float parDelta, const MouseInput& parMouse );
	const GameState* GetGameState ( void ) const { return m_state; }

private:
	bool SwapCells ( const ushort2& parFrom, const ushort2& parTo );

	GameState* const m_state;
	ushort2 m_swappingFrom;
	bool m_isSwapping;
};

#endif

