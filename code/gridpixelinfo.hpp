#ifndef id6F38D96E34914CF992745864DFF54044
#define id6F38D96E34914CF992745864DFF54044

struct GridPixelInfo {
	uint2 topLeft;
	uint2 cellCount;
	uint8_t cellSize;
};

#endif
