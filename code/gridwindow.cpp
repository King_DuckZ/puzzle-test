#include "main.hpp"
#include "gridwindow.hpp"
#include "state.hpp"
#include "gridpixelinfo.hpp"
#include <algorithm>
#include <functional>
#include <iterator>

#if !defined(Assert)
#define Assert(a) assert(a)
#define UNDEF_ASSERT
#endif
#include "DuckCore/Functor.hpp"
#if defined(UNDEF_ASSERT)
#undef UNDEF_ASSERT
#undef Assert
#endif

namespace {
	typedef duckcore::Functor2<uint8_t, uint32_t, ushort2> Func2ArgsType;
	typedef std::binder1st<Func2ArgsType> GetAtSubposType;

	const uint8_t InvalidValue = 0xFF;

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	float GetProbModifier (uint8_t parS, uint8_t parMaxS, uint16_t parSpan, GetAtSubposType parCall) {
		int32_t valueCount = 0;
		int32_t cellCount = 0;
		for (uint16_t y = 0; y <= parSpan; ++y) {
			for (uint16_t x = 0; x < parSpan; ++x) {
				ushort2 arg(x, y);
				const uint8_t currValue = parCall(arg);
				if (currValue == parS)
					++valueCount;
				if (currValue != InvalidValue)
					++cellCount;
			}
		}
		cellCount = std::max(cellCount, 1);
		const float foundMax = std::max(0.0f, static_cast<float>(cellCount - valueCount));
		const float maxS = static_cast<float>(parMaxS + 1);
		const float retVal = (foundMax / maxS) / (maxS / cellCount) + 1.0f;
		return retVal;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	float GetProbability (uint8_t parS, uint8_t parMaxS, GetAtSubposType parCall, uint8_t parSpan) {
		ushort2 coordA(0, parSpan);
		const uint8_t a = parCall(coordA);
		ushort2 coordB(parSpan - 1, parSpan);
		const uint8_t b = parCall(coordB);
		const float isDifferent = (a != b or a != parS ? 1.0f : 0.0f);
		const float incMul = (parS == a or parS == b ? 1.0f : 0.0f);
		const float incDiv = 1.0f - incMul;

		const float incVal = GetProbModifier(parS, parMaxS, parSpan, parCall);
		const float retVal = isDifferent * (incVal * incMul + (1.0f / incVal) * incDiv);
		return retVal;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	int2 GetCastedGridSize (const GameState& parState) {
		const uint2& cellCount = parState.GetGridInfo().cellCount;
		return int2(static_cast<int32_t>(cellCount.x()), static_cast<int32_t>(cellCount.y()));
	}
} //unnamed namespace

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
GridWindow::GridWindow (const std::vector<uint8_t>& parVector, const GameState& parState, const ushort2& parCenter, uint16_t parSpan, uint8_t parMaxValue) :
	m_table(parVector),
	m_state(parState),
	m_center(parCenter.x(), parCenter.y()),
	m_span(parSpan),
	m_maxCellValue(parMaxValue)
{
	assert(m_maxProbab < 0xFF);
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
uint8_t GridWindow::GetAtSubpos (uint32_t parSubgroup, ushort2 parPos) const {
	assert(parPos.x() < m_span and parPos.y() <= m_span);

	const int2 desiredCell(GetRealSubpos(parSubgroup, parPos));

	if (desiredCell >= int2(0) and desiredCell < GetCastedGridSize(m_state)) {
		return GetGemAtRealPosition(desiredCell);
	}
	else {
		return InvalidValue;
	}
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
void GridWindow::GetProbabilities (std::vector<float>& parDest) const {
	parDest.clear();
	parDest.resize(m_maxCellValue + 1);
	std::fill(parDest.begin(), parDest.end(), 0.0f);
	std::vector<float> tabLocalProbabs;
	tabLocalProbabs.reserve(m_maxCellValue + 1);

	for (uint32_t tab = 0; tab < 4; ++tab) {
		GetAtSubposType getter(std::bind1st(duckcore::Functor2<uint8_t, uint32_t, ushort2>(&GridWindow::GetAtSubpos, *this), tab));
		float probDivisor = 0.0f;
		for (uint8_t z = 0; z <= m_maxCellValue; ++z) {
			const float currProb = GetProbability(z, m_maxCellValue, getter, m_span);
			tabLocalProbabs.push_back(currProb);
			probDivisor += currProb;
		}
		for (uint8_t z = 0; z <= m_maxCellValue; ++z) {
			const float currProb = tabLocalProbabs[z] / probDivisor;
			parDest[z] += currProb * (1.0f / 4.0f);
		}
	}

	//Something is slightly wrong, so I need to normalize here
	{
		float sum = 0.0f;
		for (int z = 0; z < parDest.size(); ++z) {
			sum += parDest[z];
		}
		for (int z = 0; z < parDest.size(); ++z) {
			parDest[z] /= sum;
		}
	}
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
uint32_t GridWindow::GetDuplicateGems (std::vector<ushort2>& parOut) const {
	const uint8_t currGem = GetGemAtRealPosition(m_center);
	if (0xFF == currGem)
		return 0;

	uint32_t matchSides = 0;
	std::vector<ushort2> storedPositions;
	storedPositions.reserve(m_span);
	for (uint32_t z = 0; z < 4; ++z) {
		storedPositions.clear();
		for (uint16_t c = 0; c < m_span; ++c) {
			assert(c >= m_span - 1);
			const int2 realCoordinates(GetRealSubpos(z, ushort2(m_span - 1 - c, m_span)));
			if (realCoordinates >= int2(0) and realCoordinates < GetCastedGridSize(m_state) and GetGemAtRealPosition(realCoordinates) == currGem) {
				storedPositions.push_back(ushort2(static_cast<uint16_t>(realCoordinates.x()), static_cast<uint16_t>(realCoordinates.y())));
			}
			else {
				break;
			}
		}
		const bool match = (storedPositions.size() >= 2);
		if (match) {
			std::copy(storedPositions.begin(), storedPositions.end(), std::back_inserter(parOut));
			++matchSides;
		}
	}
	return matchSides;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
int2 GridWindow::GetRealSubpos (uint32_t parSubgroup, ushort2 parPos) const {
	bool validCell = true;
	int2 desiredCell;
	switch (parSubgroup) {
	case 0: //Top Left, vertical
		desiredCell.x() = m_center.x() - m_span + parPos.x();
		desiredCell.y() = m_center.y() - m_span + parPos.y();
		break;

	case 1: //Top Right, horizontal
		desiredCell.x() = m_center.x() + m_span - parPos.y();
		desiredCell.y() = m_center.y() - m_span + parPos.x();
		break;

	case 2: //Bottam Right, vertical
		desiredCell.x() = m_center.x() + m_span - parPos.x();
		desiredCell.y() = m_center.y() + m_span - parPos.y();
		break;

	case 3: //Bottom Left, horizontal
		desiredCell.x() = m_center.x() - m_span + parPos.y();
		desiredCell.y() = m_center.y() + m_span - parPos.x();
		break;

	default:
		validCell = false;
	}

	assert(validCell);
	if (validCell)
		return desiredCell;
	else
		return int2(-1);
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
uint8_t GridWindow::GetGemAtRealPosition (const int2& parPos) const {
	assert(parPos >= int2(0) and parPos < GetCastedGridSize(m_state));
	return m_state.GetCellValue(static_cast<uint16_t>(parPos.x()), static_cast<uint16_t>(parPos.y()));
}
