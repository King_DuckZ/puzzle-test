#ifndef id51E755F00A3E466D908E332363C8D87E
#define id51E755F00A3E466D908E332363C8D87E

class GameState;

class GridWindow {
public:
	GridWindow ( const std::vector<uint8_t>& parVector, const GameState& parState, const ushort2& parCenter, uint16_t parSpan, uint8_t parMaxValue );
	~GridWindow ( void ) = default;

	void GetProbabilities ( std::vector<float>& parDest ) const;
	uint32_t GetDuplicateGems ( std::vector<ushort2>& parOut ) const;

private:
	uint8_t GetAtSubpos ( uint32_t parSubgroup, ushort2 parPos ) const;
	int2 GetRealSubpos ( uint32_t parSubgroup, ushort2 parPos ) const;
	uint8_t GetGemAtRealPosition ( const int2& parPos ) const;

	const std::vector<uint8_t>& m_table;
	const GameState& m_state;
	const int2 m_center;
	const uint16_t m_span;
	const uint8_t m_maxCellValue;
};

#endif
