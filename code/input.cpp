#include "main.hpp"
#include "input.hpp"
#include "eventwrapper.hpp"

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
MouseInput::MouseInput() :
	m_position(0),
	m_leftButtonState(false),
	m_leftButtonDown(false),
	m_leftButtonUp(false)
{
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
MouseInput::~MouseInput() {
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
bool MouseInput::DoEvents (const EventWrapper& parEvent) {
	m_leftButtonUp = false;
	m_leftButtonDown = false;

	switch (parEvent.event.type) {
	case SDL_MOUSEMOTION:
		m_position.x() = static_cast<uint32_t>(parEvent.event.motion.x);
		m_position.y() = static_cast<uint32_t>(parEvent.event.motion.y);
		break;

	case SDL_MOUSEBUTTONDOWN:
		m_position.x() = static_cast<uint32_t>(parEvent.event.motion.x);
		m_position.y() = static_cast<uint32_t>(parEvent.event.motion.y);
		if (parEvent.event.button.button == SDL_BUTTON_LEFT) {
			m_leftButtonState = true;
			m_leftButtonDown = true;
		}
		break;

	case SDL_MOUSEBUTTONUP:
		m_position.x() = static_cast<uint32_t>(parEvent.event.motion.x);
		m_position.y() = static_cast<uint32_t>(parEvent.event.motion.y);
		if (parEvent.event.button.button == SDL_BUTTON_LEFT) {
			m_leftButtonState = false;
			m_leftButtonUp = true;
		}
		break;
	}
	return false;
}
