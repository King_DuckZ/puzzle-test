#ifndef idD9112692592140819C0F58EFED2342B8
#define idD9112692592140819C0F58EFED2342B8

struct EventWrapper;

class MouseInput {
public:
	MouseInput ( void );
	~MouseInput ( void );

	bool DoEvents ( const EventWrapper& parEvent );
	bool GetLeftButtonState ( void ) const { return m_leftButtonState; }
	bool GetLeftButtonPressed ( void ) const { return m_leftButtonDown; }
	bool GetLeftButtonReleased ( void ) const { return m_leftButtonUp; }
	const uint2& GetPosition ( void ) const { return m_position; }

private:
	uint2 m_position;
	bool m_leftButtonState;
	bool m_leftButtonDown;
	bool m_leftButtonUp;
};

#endif

