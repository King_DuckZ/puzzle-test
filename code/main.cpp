#include "main.hpp"
#include <unistd.h>
#include <ctime>
#include <X11/Xutil.h>
#include "game.hpp"
#include "sdlrenderer.hpp"
#include "eventmanager.hpp"
#include "input.hpp"
#include "gridpixelinfo.hpp"
#include <cstdlib>

namespace {
	const int32_t ScreenWidth = 755;
	const int32_t ScreenHeight = 600;
	const float FrameDuration = 1.0f / 60.0f;

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	float GetDeltaTime (const timespec& parStart, const timespec& parEnd) {
		timespec delta;
		if (parStart.tv_nsec > parEnd.tv_nsec) {
			delta.tv_sec = parEnd.tv_sec - parStart.tv_sec - 1;
			delta.tv_nsec = 1000000000 + parEnd.tv_nsec - parStart.tv_nsec;
		}
		else {
			delta.tv_sec = parEnd.tv_sec - parStart.tv_sec;
			delta.tv_nsec = parEnd.tv_nsec - parStart.tv_nsec;
		}
		return static_cast<float>(delta.tv_sec) + static_cast<float>(delta.tv_nsec) / 1000000000.0f;
	}

} //unnamed namesapace

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
int main() {
	std::srand(std::time(nullptr));

	GridPixelInfo gridInfo;
	gridInfo.topLeft = uint2(329, 99);
	gridInfo.cellCount = uint2(8);
	gridInfo.cellSize = 35;

	SDLRenderer renderer(uint2(ScreenWidth, ScreenHeight), &gridInfo);
	const int retVal = renderer.Init(GAME_NAME);
	if (retVal)
		return 1;
	Game game(&gridInfo, SDLRenderer::GemCount);
	EventManager eventMan;
	MouseInput minput;

	eventMan.RegisterCallback(EventManager::DoEventsCallbackType(&SDLRenderer::DoEvents, renderer));
	eventMan.RegisterCallback(EventManager::DoEventsCallbackType(&MouseInput::DoEvents, minput));

	bool running;
	timespec frameStartTime;
	timespec frameStopTime;
	clock_gettime(CLOCK_MONOTONIC, &frameStartTime);
	do {
		frameStopTime = frameStartTime;
		clock_gettime(CLOCK_MONOTONIC, &frameStartTime);
		const float deltaTime = GetDeltaTime(frameStopTime, frameStartTime);

		running = not eventMan.DoEvents();
		running &= game.UpdateFrame(deltaTime, minput);
		renderer.UpdateScreen(game.GetGameState());

		timespec timeNow;
		clock_gettime(CLOCK_MONOTONIC, &timeNow);
		const float currFrameDuration = GetDeltaTime(frameStartTime, timeNow);
		if (currFrameDuration < FrameDuration) {
			const unsigned int sleepDuration = static_cast<unsigned int>((FrameDuration - currFrameDuration) * 1000000.0f);
			usleep(sleepDuration);
		}
	} while (running);

	std::cout << "Done, bye!" << std::endl;
	return 0;
}
