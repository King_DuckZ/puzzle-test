#include "main.hpp"
#include "sdlrenderer.hpp"
#include <SDL.h>
#include <SDL_image.h>
#include <cstdlib>
#include <iostream>
#include "state.hpp"
#include "eventwrapper.hpp"
#include "gridpixelinfo.hpp"

namespace {
	const uint8_t GemsCount = 5;
}

const uint8_t SDLRenderer::GemCount = GemsCount;
struct GameResources {
	SDL_Surface* wallpaper;
	SDL_Surface* gems[GemsCount];
};

namespace {
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	SDL_Surface* LoadSingleImage (const char* parPath) {
		assert(nullptr != parPath);
		SDL_RWops* rwop;
		rwop = SDL_RWFromFile(parPath, "rb");
		return IMG_LoadPNG_RW(rwop);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool LoadResources (GameResources* parResourcesOut) {
		assert(nullptr != parResourcesOut);
		static const char* gemResources[GemsCount] = {
			"graphics/Blue.png", "graphics/Green.png", "graphics/Purple.png",
			"graphics/Red.png", "graphics/Yellow.png"
		};

		if ((parResourcesOut->wallpaper = LoadSingleImage("graphics/BackGround.png")) == nullptr) {
			std::cerr << "Error loading graphics/BackGround.png" << std::endl;
			return false;
		}

		for (int z = 0; z < GemsCount; ++z) {
			if ((parResourcesOut->gems[z] = LoadSingleImage(gemResources[z])) == nullptr) {
				std::cerr << "Error loading " << gemResources[z] << std::endl;
				return false;
			}
		}
		return true;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void DrawGems (const uint2& parFrom, uint8_t parGemSize, const uint2& parArea, SDL_Surface* parDest, SDL_Surface* parGems[GemsCount], const GameState* parState) {
		assert(parState);
		const uint2 gridSize = parState->GetGridSize();
		for (uint16_t y = 0; y < gridSize.y(); ++y) {
			for (uint16_t x = 0; x < gridSize.x(); ++x) {
				const uint8_t gemType = parState->GetCellValue(x, y);
				assert(gemType < GemsCount);
				SDL_Rect gemPlace = {
					static_cast<Sint16>(parFrom.x() + x * parGemSize),
					static_cast<Sint16>(parFrom.y() + y * parGemSize),
					parGemSize,
					parGemSize
				};
				if (0xFF != gemType)
					SDL_BlitSurface(parGems[gemType], nullptr, parDest, &gemPlace);
			}
		}
	}
} //unnamed namespace

struct SDLRenderer::LocalData {
	GameResources resources;
	SDL_Surface* screen;
};

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
SDLRenderer::SDLRenderer (const uint2& parScreenSize, const GridPixelInfo* parGridInfo) :
	m_screenSize(parScreenSize),
	m_local(new LocalData),
	m_gridInfo(parGridInfo)
{
	assert(m_gridInfo);
	m_local->screen = nullptr;
	m_local->resources.wallpaper = nullptr;
	for (int z = 0; z < GemsCount; ++z)
		m_local->resources.gems[z] = nullptr;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
SDLRenderer::~SDLRenderer() {
	SDL_Quit();
	CleanupResources();
	delete m_local;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
void SDLRenderer::UpdateScreen (const GameState* parState) {
	SDL_BlitSurface(m_local->resources.wallpaper, nullptr, m_local->screen, nullptr);
	DrawGems(m_gridInfo->topLeft, m_gridInfo->cellSize, uint2(344, 344), m_local->screen, m_local->resources.gems, parState);
	SDL_Flip(m_local->screen);
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
int SDLRenderer::Init (const char* parWinTitle) {
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cerr << "Unable to initialize SDL: " << SDL_GetError() << std::endl;
		return 1;
	}

	const uint32_t flags = SDL_DOUBLEBUF /*| SDL_FULLSCREEN*/;
	m_local->screen = SDL_SetVideoMode(m_screenSize.x(), m_screenSize.y(), 32, flags);
	if (m_local->screen == NULL) {
		std::cerr << "Unable to set video mode: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_WM_SetCaption(parWinTitle, "Icon");
	if (not LoadResources(&m_local->resources)) {
		std::cerr << "Unable to load some resources" << std::endl;
		CleanupResources();
		return 1;
	}
	return 0;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
void SDLRenderer::CleanupResources() {
	if (m_local->resources.wallpaper) {
		SDL_FreeSurface(m_local->resources.wallpaper);
		m_local->resources.wallpaper = nullptr;
	}
	for (int z = 0; z < GemsCount; ++z) {
		if (m_local->resources.gems[z]) {
			SDL_FreeSurface(m_local->resources.gems[z]);
			m_local->resources.gems[z] = nullptr;
		}
	}
	if (m_local->screen) {
		SDL_FreeSurface(m_local->screen);
		m_local->screen = nullptr;
	}
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
bool SDLRenderer::DoEvents (const EventWrapper& parEvent) {
	return false;
}
