#ifndef idD0E34E84EACE4225982A57F6E1B8DD8F
#define idD0E34E84EACE4225982A57F6E1B8DD8F

class GameState;
struct EventWrapper;
struct GridPixelInfo;

class SDLRenderer {
public:
	SDLRenderer ( const uint2& parScreenSize, const GridPixelInfo* parGridInfo );
	~SDLRenderer ( void );

	int Init ( const char* parWinTitle );
	void UpdateScreen ( const GameState* parState );
	bool DoEvents ( const EventWrapper& parEvent );

	static const uint8_t GemCount;

private:
	struct LocalData;

	void CleanupResources ( void );

	uint2 m_screenSize;
	LocalData* const m_local;
	const GridPixelInfo* const m_gridInfo;
};

#endif
