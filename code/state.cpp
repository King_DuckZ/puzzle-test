#include "main.hpp"
#include "state.hpp"
#include "gridpixelinfo.hpp"
#include <cstdlib>
#include "gridwindow.hpp"

namespace {
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	uint32_t GetCellIndex (const ushort2& parCell, const uint2& parCellCount) {
		assert(parCell.x() < parCellCount.x());
		assert(parCell.y() < parCellCount.y());
		const uint32_t index = parCellCount.x() * parCell.y() + parCell.x();
		assert(index < m_table.size());
		return index;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool GetRandBool() {
		return ((std::rand() & 1) == 1);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void SetInitialSwappableCells (std::vector<uint8_t>& parTable, const ushort2& parMiddle, const ushort2& parMul, uint8_t parType, const uint2& parCellCount) {
		assert((parMul.x() == 1) xor (parMul.y() == 1));
		assert(parMiddle + parMul < ushort2(static_cast<uint16_t>(parCellCount.x()), static_cast<uint16_t>(parCellCount.y())));
		assert(parMiddle >= parMul);

		const uint32_t indexMiddle = GetCellIndex(parMiddle, parCellCount);
		parTable[indexMiddle] = parType;

		const ushort2 prev(parMiddle - ushort2(1) * parMul);
		const uint32_t indexPrev = GetCellIndex(prev, parCellCount);
		parTable[indexPrev] = parType;

		const ushort2 next(parMiddle + ushort2(1) * parMul);
		const uint32_t indexNext = GetCellIndex(next, parCellCount);
		parTable[indexNext] = parType;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	ushort2 GetSwapInAvailableDirection(const ushort2& parMiddle, const ushort2& parMul, const uint2& parCellCount) {
		assert((parMul.x() == 1) xor (parMul.y() == 1));
		ushort2 target;
		const ushort2 cellCount(static_cast<uint16_t>(parCellCount.x()), static_cast<uint16_t>(parCellCount.y()));
		assert(parMiddle < cellCount);
		const bool canSum = static_cast<bool>(parMiddle + parMul < cellCount);
		const bool canSub = static_cast<bool>(parMiddle >= parMul);

		assert(canSum or canSub);
		const bool doSum = not (canSum xor canSub) and GetRandBool() or canSum;
#if defined(NDEBUG)
		{
			std::cout << "Operating on (" << parMiddle << " +- " << parMul << ")\n";
			if (canSum and canSub)
				std::cout << "Both sum and sub would be ok\n";
			if (doSum)
				std::cout << "I'm summing\n";
			else
				std::cout << "I'm subbing\n";
		}
#endif
		if (doSum)
			return parMiddle + parMul;
		else
			return parMiddle - parMul;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	uint8_t GetRandom (const std::vector<float>& parProbabs) {
		assert(not parProbabs.empty());
		const uint32_t valCount = parProbabs.size();

		typedef std::pair<uint8_t, float> SortedType;
		std::vector<SortedType> sortedProbabs;
		sortedProbabs.reserve(valCount);
		for (uint32_t z = 0; z < valCount; ++z) {
			sortedProbabs.push_back(SortedType(static_cast<uint8_t>(z), parProbabs[z]));
		}
		std::sort(sortedProbabs.begin(), sortedProbabs.end(), [](const SortedType& a, const SortedType& b)->bool {return a.second < b.second;});

		const float randVal = static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX);
		float sum = 0.0f;
		for (auto itCurrP = sortedProbabs.begin(), itCurrPEnd = sortedProbabs.end(); itCurrP != itCurrPEnd; ++itCurrP) {
			sum += itCurrP->second;
			if (randVal <= sum) {
				return itCurrP->first;
			}
		}
		std::cout << "GetRandom() failed, returning 0 :(\n";
		return 0;
	}
} //unnamed namespace

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
bool PointIsInGrid (const GridPixelInfo& parGridInfo, const uint2& parCoords) {
	const uint2 bottomRight(parGridInfo.topLeft + parGridInfo.cellCount * static_cast<uint32_t>(parGridInfo.cellSize));
	return parGridInfo.topLeft <= parCoords and bottomRight > parCoords;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
ushort2 GetCellFromCoordinates (const GridPixelInfo& parGridInfo, const uint2& parCoords) {
	assert(PointIsInGrid(parGridInfo, parCoords));

	const uint2 result((parCoords - parGridInfo.topLeft) / static_cast<uint32_t>(parGridInfo.cellSize));
	return ushort2(static_cast<uint16_t>(result.x()), static_cast<uint16_t>(result.y()));
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
GameState::GameState (const GridPixelInfo* parGridInfo, uint8_t parGemCount) :
	m_gridInfo(parGridInfo),
	m_gemCount(parGemCount)
{
	assert(m_gridInfo);
	m_table.resize(m_gridInfo->cellCount.area());
	std::fill(m_table.begin(), m_table.end(), 0);
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
GameState::~GameState() {
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
void GameState::FillRandom() {
	assert(m_gridInfo.cellCount >= uint2(3));

	if (m_gridInfo->cellCount >= uint2(3)) {
		ushort2 direction(0);
		ushort2 pickedCell;
		const uint2& wh = m_gridInfo->cellCount;
		std::fill(m_table.begin(), m_table.end(), 0xFF);
		if (GetRandBool()) {
			pickedCell = ushort2(static_cast<uint16_t>(std::rand() % wh.x()), static_cast<uint16_t>(std::rand() % (wh.y() - 2)) + 1);
			m_table[GetCellIndex(pickedCell, wh)] = static_cast<uint8_t>(std::rand() % m_gemCount);
			std::cout << "Cell " << pickedCell << " and the surrounding vertical cells will be set to " << static_cast<uint32_t>(GetCellValue(pickedCell.x(), pickedCell.y())) << "\n";
			direction.y() = 1;
		}
		else {
			pickedCell = ushort2(static_cast<uint16_t>(std::rand() % (wh.x() - 2)) + 1, static_cast<uint16_t>(std::rand() % wh.y()));
			m_table[GetCellIndex(pickedCell, wh)] = static_cast<uint8_t>(std::rand() % m_gemCount);
			std::cout << "Cell " << pickedCell << " and the surrounding horizontal cells will be set to " << static_cast<uint32_t>(GetCellValue(pickedCell.x(), pickedCell.y())) << "\n";
			direction.x() = 1;
		}
		const uint8_t gemType = static_cast<uint8_t>(std::rand() % m_gemCount);
		SetInitialSwappableCells(m_table, pickedCell, direction, gemType, m_gridInfo->cellCount);
		const ushort2 swapCell((GetRandBool() ? pickedCell + direction : pickedCell - direction));
		const ushort2 swapWith = GetSwapInAvailableDirection(swapCell, ushort2(direction.y(), direction.x()), m_gridInfo->cellCount);
#if defined(NDEBUG)
		std::cout << "Swapping " << swapCell << " with " << swapWith << "\n";
#endif
		this->SwapCells(swapCell, swapWith);
		FillEmptyCells();
	}
	else {
		const uint8_t gemCount = m_gemCount;
		std::generate(m_table.begin(), m_table.end(), [gemCount]()->uint8_t {return std::rand() % gemCount;});
	}
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
uint8_t GameState::GetCellValue (uint16_t parX, uint16_t parY) const {
	const uint32_t index = GetCellIndex(ushort2(parX, parY), m_gridInfo->cellCount);
	return m_table[index];
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
const uint2& GameState::GetGridSize() const {
	return m_gridInfo->cellCount;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
void GameState::SwapCells (const ushort2 parFrom, const ushort2 parTo) {
	const uint32_t indexFrom = GetCellIndex(parFrom, m_gridInfo->cellCount);
	const uint32_t indexTo = GetCellIndex(parTo, m_gridInfo->cellCount);
	std::swap(m_table[indexFrom], m_table[indexTo]);
	while (RemoveSequences()) {
		FillEmptyCells();
	}
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
bool GameState::RemoveSequences() {
	ushort2 currCell;
	const uint16_t largestGridSide = static_cast<uint16_t>(std::max(m_gridInfo->cellCount.x(), m_gridInfo->cellCount.y()));
	const uint2& cellCount = m_gridInfo->cellCount;
	std::vector<ushort2> remove;
	for (currCell.y() = 0; currCell.y() < static_cast<uint16_t>(cellCount.y()); ++currCell.y()) {
		for (currCell.x() = 0; currCell.x() < static_cast<uint16_t>(cellCount.x()); ++currCell.x()) {
			GridWindow win(m_table, *this, currCell, largestGridSide - 1, m_gemCount - 1);
			win.GetDuplicateGems(remove);
		}
	}
	for (auto itRm = remove.begin(), itRmEnd = remove.end(); itRm != itRmEnd; ++itRm) {
		const uint32_t index = GetCellIndex(*itRm, cellCount);
		m_table[index] = 0xFF;
	}

	if (not remove.empty()) {
	}
	return not remove.empty();
}

///------------------------------------------------------------------------
///------------------------------------------------------------------------
void GameState::FillEmptyCells() {
	std::vector<float> probabs(m_gemCount - 1);
	ushort2 currCell;
	for (currCell.y() = 0; currCell.y() < static_cast<uint16_t>(m_gridInfo->cellCount.y()); ++currCell.y()) {
		for (currCell.x() = 0; currCell.x() < static_cast<uint16_t>(m_gridInfo->cellCount.x()); ++currCell.x()) {
			if (0xFF != GetCellValue(currCell.x(), currCell.y()))
				continue;
			GridWindow window(m_table, *this, currCell, 2, m_gemCount - 1);
			window.GetProbabilities(probabs);
			const uint32_t index = GetCellIndex(currCell, m_gridInfo->cellCount);
			//std::cout << "Filling cell " << index << " (" << currCell << ")\n";
			m_table[index] = GetRandom(probabs);
		}
	}
}
