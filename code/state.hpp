#ifndef idCC8588086A7C4698B09B3A3CC40EC229
#define idCC8588086A7C4698B09B3A3CC40EC229

struct GridPixelInfo;

class GameState {
public:
	GameState ( const GridPixelInfo* parGridInfo, uint8_t parGemCount );
	~GameState ( void );

	void FillRandom ( void );
	const uint2& GetGridSize ( void ) const;
	uint8_t GetCellValue ( uint16_t parX, uint16_t parY ) const;
	void SwapCells ( const ushort2 parFrom, const ushort2 parTo );
	const GridPixelInfo& GetGridInfo ( void ) const { return *m_gridInfo; }

private:
	bool RemoveSequences ( void );
	void FillEmptyCells ( void );

	std::vector<uint8_t> m_table;
	const GridPixelInfo* const m_gridInfo;
	const uint8_t m_gemCount;
};

ushort2 GetCellFromCoordinates ( const GridPixelInfo& parGridInfo, const uint2& parCoords );
bool PointIsInGrid ( const GridPixelInfo& parGridInfo, const uint2& parCoords );

#endif
