#ifndef id0ED8AF63595D41E4A82337D9266A8E17
#define id0ED8AF63595D41E4A82337D9266A8E17

template <typename T, uint32_t S>
class VectorMem {
protected:
	VectorMem ( void ) { }
	VectorMem ( const VectorMem& parOther ) { std::copy(parOther.m_memory, parOther.m_memory + S, m_memory); }
	explicit VectorMem ( const T& parInit ) { std::fill(m_memory, m_memory + S, parInit); }
	~VectorMem ( void ) { }

	const VectorMem& operator+= ( const VectorMem& parOther );
	const VectorMem& operator-= ( const VectorMem& parOther );
	const VectorMem& operator*= ( const VectorMem& parOther );
	const VectorMem& operator/= ( const VectorMem& parOther );
	const VectorMem& operator+= ( T parOther );
	const VectorMem& operator-= ( T parOther );
	const VectorMem& operator*= ( T parOther );
	const VectorMem& operator/= ( T parOther );

	T m_memory[S];
};

template <typename T, uint32_t S>
class Vector;

template <typename T>
class Vector<T, 1> : private VectorMem<T, 1> {
	typedef VectorMem<T, 1> ParentType;
public:
	Vector ( void ) { }
	Vector ( const Vector& parOther ) : ParentType(parOther) { }
	explicit Vector ( const T& parInit ) : ParentType(parInit) { }

	T& x ( void ) { return this->m_memory[0]; }
	const T& x ( void ) const { return this->m_memory[0]; }

	T& operator[] ( uint32_t parIndex ) { assert(parIndex < 1); return this->m_memory[parIndex]; }
	const T& operator[] ( uint32_t parIndex ) const { assert(parIndex < 1); return this->m_memory[parIndex]; }

	const Vector& operator+= ( const Vector& parOther ) { ParentType::operator+=(parOther); return *this; }
	const Vector& operator-= ( const Vector& parOther ) { ParentType::operator-=(parOther); return *this; }
	const Vector& operator*= ( const Vector& parOther ) { ParentType::operator*=(parOther); return *this; }
	const Vector& operator/= ( const Vector& parOther ) { ParentType::operator/=(parOther); return *this; }
	const Vector& operator+= ( T parOther ) { ParentType::operator+=(parOther); return *this; }
	const Vector& operator-= ( T parOther ) { ParentType::operator-=(parOther); return *this; }
	const Vector& operator*= ( T parOther ) { ParentType::operator*=(parOther); return *this; }
	const Vector& operator/= ( T parOther ) { ParentType::operator/=(parOther); return *this; }
};

template <typename T>
class Vector<T, 2> : private VectorMem<T, 2> {
	typedef VectorMem<T, 2> ParentType;
public:
	Vector ( void ) { }
	Vector ( const Vector& parOther ) : ParentType(parOther) { }
	explicit Vector ( const T& parInit ) : ParentType(parInit) { }
	Vector ( const T& parX, const T& parY ) { this->m_memory[0] = parX; this->m_memory[1] = parY; }

	T& x ( void ) { return this->m_memory[0]; }
	const T& x ( void ) const { return this->m_memory[0]; }
	T& y ( void ) { return this->m_memory[1]; }
	const T& y ( void ) const { return this->m_memory[1]; }

	T area ( void ) const { return x() * y(); }

	T& operator[] ( uint32_t parIndex ) { assert(parIndex < 2); return this->m_memory[parIndex]; }
	const T& operator[] ( uint32_t parIndex ) const { assert(parIndex < 2); return this->m_memory[parIndex]; }

	const Vector& operator+= ( const Vector& parOther ) { ParentType::operator+=(parOther); return *this; }
	const Vector& operator-= ( const Vector& parOther ) { ParentType::operator-=(parOther); return *this; }
	const Vector& operator*= ( const Vector& parOther ) { ParentType::operator*=(parOther); return *this; }
	const Vector& operator/= ( const Vector& parOther ) { ParentType::operator/=(parOther); return *this; }
	const Vector& operator+= ( T parOther ) { ParentType::operator+=(parOther); return *this; }
	const Vector& operator-= ( T parOther ) { ParentType::operator-=(parOther); return *this; }
	const Vector& operator*= ( T parOther ) { ParentType::operator*=(parOther); return *this; }
	const Vector& operator/= ( T parOther ) { ParentType::operator/=(parOther); return *this; }
};

//typedef Vector<float, 2> float2;
typedef Vector<int32_t, 2> int2;
typedef Vector<uint32_t, 2> uint2;
typedef Vector<uint16_t, 2> ushort2;

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
template <typename T, uint32_t S>
const VectorMem<T, S>& VectorMem<T, S>::operator+= (const VectorMem& parOther) {
	for (uint32_t z = 0; z < S; ++z)
		m_memory[z] += parOther.m_memory[z];
	return *this;
}
template <typename T, uint32_t S>
const VectorMem<T, S>& VectorMem<T, S>::operator-= (const VectorMem& parOther) {
	for (uint32_t z = 0; z < S; ++z)
		m_memory[z] -= parOther.m_memory[z];
	return *this;
}
template <typename T, uint32_t S>
const VectorMem<T, S>& VectorMem<T, S>::operator*= (const VectorMem& parOther) {
	for (uint32_t z = 0; z < S; ++z)
		m_memory[z] *= parOther.m_memory[z];
	return *this;
}
template <typename T, uint32_t S>
const VectorMem<T, S>& VectorMem<T, S>::operator/= (const VectorMem& parOther) {
	for (uint32_t z = 0; z < S; ++z)
		m_memory[z] /= parOther.m_memory[z];
	return *this;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
template <typename T, uint32_t S>
const VectorMem<T, S>& VectorMem<T, S>::operator+= (T parOther) {
	for (uint32_t z = 0; z < S; ++z)
		m_memory[z] += parOther;
	return *this;
}
template <typename T, uint32_t S>
const VectorMem<T, S>& VectorMem<T, S>::operator-= (T parOther) {
	for (uint32_t z = 0; z < S; ++z)
		m_memory[z] -= parOther;
	return *this;
}
template <typename T, uint32_t S>
const VectorMem<T, S>& VectorMem<T, S>::operator*= (T parOther) {
	for (uint32_t z = 0; z < S; ++z)
		m_memory[z] *= parOther;
	return *this;
}
template <typename T, uint32_t S>
const VectorMem<T, S>& VectorMem<T, S>::operator/= (T parOther) {
	for (uint32_t z = 0; z < S; ++z)
		m_memory[z] /= parOther;
	return *this;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
template <typename T, uint32_t S>
inline
Vector<T, S> operator+ (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	Vector<T, S> retVal(parLeft);
	retVal += parRight;
	return retVal;
}
template <typename T, uint32_t S>
inline
Vector<T, S> operator- (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	Vector<T, S> retVal(parLeft);
	retVal -= parRight;
	return retVal;
}
template <typename T, uint32_t S>
inline
Vector<T, S> operator* (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	Vector<T, S> retVal(parLeft);
	retVal *= parRight;
	return retVal;
}
template <typename T, uint32_t S>
inline
Vector<T, S> operator/ (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	Vector<T, S> retVal(parLeft);
	retVal /= parRight;
	return retVal;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
template <typename T, uint32_t S>
inline
Vector<T, S> operator+ (const Vector<T, S>& parLeft, T parRight) {
	Vector<T, S> retVal(parLeft);
	retVal += parRight;
	return retVal;
}
template <typename T, uint32_t S>
inline
Vector<T, S> operator- (const Vector<T, S>& parLeft, T parRight) {
	Vector<T, S> retVal(parLeft);
	retVal -= parRight;
	return retVal;
}
template <typename T, uint32_t S>
inline
Vector<T, S> operator* (const Vector<T, S>& parLeft, T parRight) {
	Vector<T, S> retVal(parLeft);
	retVal *= parRight;
	return retVal;
}
template <typename T, uint32_t S>
inline
Vector<T, S> operator/ (const Vector<T, S>& parLeft, T parRight) {
	Vector<T, S> retVal(parLeft);
	retVal /= parRight;
	return retVal;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
template <typename T, uint32_t S>
inline
bool operator< (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	for (uint32_t z = 0; z < S; ++z) {
		if (parLeft[z] >= parRight[z])
			return false;
	}
	return true;
}
template <typename T, uint32_t S>
inline
bool operator> (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	for (uint32_t z = 0; z < S; ++z) {
		if (parLeft[z] <= parRight[z])
			return false;
	}
	return true;
}
template <typename T, uint32_t S>
inline
bool operator<= (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	for (uint32_t z = 0; z < S; ++z) {
		if (parRight[z] < parLeft[z])
			return false;
	}
	return true;
}
template <typename T, uint32_t S>
inline
bool operator>= (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	for (uint32_t z = 0; z < S; ++z) {
		if (parLeft[z] < parRight[z])
			return false;
	}
	return true;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
template <typename T, uint32_t S>
inline
bool operator== (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	for (uint32_t z = 0; z < S; ++z) {
		if (not (parLeft[z] == parRight[z]))
			return false;
	}
	return true;
}
template <typename T, uint32_t S>
inline
bool operator!= (const Vector<T, S>& parLeft, const Vector<T, S>& parRight) {
	return not (parLeft == parRight);
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
template <typename T, uint32_t S>
inline
std::ostream& operator<< ( std::ostream& parStream, const Vector<T, S>& parVector ) {
	parStream << "<";
	for (uint32_t z = 0; z < S - 1; ++z) {
		parStream << parVector[z] << ", ";
	}
	parStream << parVector[S - 1] << ">";
	return parStream;
}

#endif
